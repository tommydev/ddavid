var express = require('express');
var sql = require('../../services/post-service');
var router = express.Router();
var moment = require('moment');
var xFrameOptions = require('x-frame-options');
router.use(xFrameOptions());
	/* GET posts listing. */
router.get('/', function(req, res, next) {
	res.get('X-Frame-Options');
	console.log("USER: " + JSON.stringify(req.sess));
	if (!req.sess.user) {
		res.redirect("/cms/users/login");
	}
	else {
		if (req.sess.userLevel > 2) {
			//Order posts by date desc
			var userName = req.sess.userName;
			var getData = 'select * FROM d_posts ORDER by post_id DESC LIMIT 10';
			sql.query(getData, function(results) {
				sql.query("SELECT COUNT(*) FROM d_posts;", function(postcount) {
					sql.query("SELECT * FROM d_history ORDER by ID desc LIMIT 10", function(hists) {
						sql.query("SELECT * FROM d_messages ORDER by ID desc LIMIT 10", function(msgs) {
							console.log('Got all posts');
							console.log(JSON.stringify(results));
							for (var i = 0; i < results.length; i++) {
								results[i].dateCreated = moment(results[i].dateCreated).format('MMMM Do YYYY, hh:mm');
							}
							for (var i = 0; i < results.length; i++) {
								results[i].dateUpdated = moment(results[i].dateUpdated).format('MMMM Do YYYY, hh:mm');
							}
							for (var i = 0; i < results.length; i++) {
								results[i].datePosted = moment(results[i].datePosted).format('hh:mm');
							}
							var vm = {
								title: "Blog Posts",
								req: req,
								posts: results,
								userName: req.sess.userName,
							};

							vm.numberofposts = postcount.length;
							vm.histories = hists;
							vm.actions = hists.length;

							vm.messages = msgs;

							res.render('cms/dashboard', vm);
						});



					});
				});
			});
		}
		else {
			res.redirect("/");
		}
	}
});

module.exports = router;